﻿namespace PersonalSpendingAnalysis.Dtos
{
    public class BudgetDto
    {
        public decimal Amount { get; set; }
        public string CategoryName { get; set; }
    }
}