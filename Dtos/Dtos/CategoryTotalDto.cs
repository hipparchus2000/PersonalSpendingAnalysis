﻿namespace PersonalSpendingAnalysis.Dtos
{
    public class CategoryTotalDto
    {
        public decimal Amount { get; set; }
        public string CategoryName { get; set; }
    }
}