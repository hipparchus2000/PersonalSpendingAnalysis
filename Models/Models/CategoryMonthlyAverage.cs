﻿
namespace PersonalSpendingAnalysis.Models
{
    public class CategoryMonthlyAverage
    {
        public decimal Amount { get; set; } 
        public string CategoryName { get; set; }
    }
}
