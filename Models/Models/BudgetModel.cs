﻿

namespace PersonalSpendingAnalysis.Models
{
    public class BudgetModel
    {
        public decimal Amount { get; set; } 
        public string CategoryName { get; set; }
    }
}
