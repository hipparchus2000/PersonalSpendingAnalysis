﻿
namespace PersonalSpendingAnalysis.Models
{
    public class CategoryTotal
    {
        public decimal Amount { get; set; } 
        public string CategoryName { get; set; }
    }
}
